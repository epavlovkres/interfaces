﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
	public class AccountService : IAccountService
	{
		IRepository<Account> _repository;

		public AccountService(IRepository<Account> repository)
		{
			_repository = repository;
		}

		public void AddAccount(Account account)
		{
			if (IsValid(account))
				_repository.Add(account);
		}

		bool IsValid(Account account)
		{
			return (account.BirthDate.AddYears(18) <= DateTime.Now)
				&& !string.IsNullOrEmpty(account.FirstName)
				&& !string.IsNullOrEmpty(account.LastName);
		}
	}
}
