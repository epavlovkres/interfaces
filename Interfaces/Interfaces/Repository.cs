﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;

namespace Interfaces
{
	public class Repository<T>: IRepository<T>
	{
		string _filePath;
		List<T> _list;

		public Repository(string filePath)
		{
			_filePath = filePath;
			var fileContent = File.ReadAllText(_filePath);
			_list = JsonSerializer.Deserialize<List<T>>(fileContent);
		}

		public void Add(T item)
		{
			_list.Add(item);
			WriteToFile();
		}

		public IEnumerable<T> GetAll()
		{
			foreach (var item in _list)
			{
				yield return item;
			}
			// or just return _list;
		}

		public T GetOne(Func<T, bool> predicate)
		{
			return _list.Find((T item) => predicate(item));
		}

		void WriteToFile()
		{
			var fileContent = JsonSerializer.Serialize<List<T>>(_list);
			File.WriteAllText(_filePath, fileContent);
		}
	}
}
