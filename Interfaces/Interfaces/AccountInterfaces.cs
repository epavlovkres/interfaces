﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Interfaces
{
	public class Account: IComparable<Account>
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public DateTime BirthDate { get; set; }

		public int CompareTo([AllowNull] Account other)
		{
			if (other == null) return 1;
			var compareNames = FirstName.CompareTo(other.FirstName);
			if (compareNames != 0)
				return compareNames;
			compareNames = LastName.CompareTo(other.LastName);
			if (compareNames != 0)
				return compareNames;
			return BirthDate.CompareTo(other.BirthDate);
		}

		public void PrintToConsole()
		{
			Console.WriteLine($"FirstName: {FirstName}, LastName: {LastName}, BirthDate: {BirthDate}");
		}
	}

	// В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, etc
	public interface IRepository<T>
	{
		// когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)
		IEnumerable<T> GetAll();
		T GetOne(Func<T, bool> predicate);
		void Add(T item);
	}

	public interface IAccountService
	{
		// В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
		// Если валидация прошла успешно, то добавлять аккаунт в репозиторий
		void AddAccount(Account account);
	}
}