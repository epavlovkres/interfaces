﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.IO;
using System.Collections;

namespace Interfaces
{
	public class JsonList<T>: IEnumerable<T>, IAlgorithm where T: IComparable<T>
	{
		List<T> _jsonContent;

		public JsonList(string filePath)
		{
			var fileContent = File.ReadAllText(filePath);
			_jsonContent = JsonSerializer.Deserialize<List<T>>(fileContent);
		}

		public IEnumerator<T> GetEnumerator()
		{
			// return _jsonContent.GetEnumerator(); or
			foreach (var item in _jsonContent)
			{
				yield return item;
			}
		}

		public void Sort()
		{
			_jsonContent.Sort();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
