﻿using System;

namespace Interfaces
{
	class Program
	{
		static void Main(string[] args)
		{
			// Task 1
			const string fileName = "example.json";
			Console.WriteLine($"List of string from {fileName}:");
			var jsonList = new JsonList<Account>(fileName);
			foreach (var account in jsonList)
			{
				account.PrintToConsole();
			}

			Console.WriteLine();

			// Task 2
			Console.WriteLine($"List of sorted strings:");
			jsonList.Sort();
			foreach (var account in jsonList)
			{
				account.PrintToConsole();
			}
		}
	}
}
