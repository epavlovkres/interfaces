﻿using System;
using Xunit;
using Interfaces;
using Moq;
using System.Collections.Generic;

namespace Interfaces.Tests
{
	public class AccountServiceTests
	{
		[Fact]
		public void AddAccountTest()
		{
			var accountList = new List<Account>();
			var mock = new Mock<IRepository<Account>>();
			mock.Setup(a => a.Add(It.IsAny<Account>())).Callback<Account>((acc) =>
			{
				accountList.Add(acc);
			});
			Account testAccount1 = new Account
			{
				FirstName = "Evgeniy", LastName ="Pavlov", BirthDate = new DateTime(1994, 9, 1)
			};
			Account testAccount2 = new Account
			{
				FirstName = "", LastName = "test", BirthDate = new DateTime(1000, 10, 2)
			};
			Account testAccount3 = new Account
			{
				FirstName = "test2",
				LastName = "test2",
				BirthDate = new DateTime(2018, 3, 3)
			};
			var accService = new AccountService(mock.Object);
			accService.AddAccount(testAccount1);
			accService.AddAccount(testAccount2);
			accService.AddAccount(testAccount3);
			Assert.Equal(1, accountList.Count);
			Assert.Equal(testAccount1, accountList[0]);
		}
	}
}
